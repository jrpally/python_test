import aylien_news_api

class AylienConfiguration:
	def __init__(self, applicationId, apiKey):
		self.configuration = aylien_news_api.Configuration()
		self.configuration.api_key['X-AYLIEN-NewsAPI-Application-ID'] = applicationId
		self.configuration.api_key['X-AYLIEN-NewsAPI-Application-Key'] = apiKey

	def getConfiguration(self):
		return self.configuration