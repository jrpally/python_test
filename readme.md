# Aylien Tests
## _Instructions_

Aylien tests are a set of test for Aylien SDK Rest Services.

## Features

- Test 60 requests Api Exception
- Test Content given a word
- Test Number of stories

To run: 
- Clone the repository https://gitlab.com/jrpally/python_test.git
```sh
git clone https://gitlab.com/jrpally/python_test.git
```
- Install requirements: 
```sh
python -m pip install -r requirements.txt
```
- Run Tests:
```sh
python aylintests.py
```
