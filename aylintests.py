import unittest
import aylienconfiguration
import ayliensdk
import pickle
import codecs
from aylien_news_api.rest import ApiException
import time

class TestMyModule(unittest.TestCase):
	def setUp(self):
		self.aylienConfiguration = aylienconfiguration.AylienConfiguration('6f0b19bf', '21ba8300d45271d57aecc36982baca16');
		self.aylienSdkLib = ayliensdk.AylienSdk(self.aylienConfiguration.getConfiguration());

	def test1_news_quantity(self):
		term = 'startup'
		news = self.aylienSdkLib.getStories('startup','NOW-7DAYS', 'NOW').stories
		self.assertEqual(len(news), 10)
	
	def test2_news_frequency(self):
		term = 'depeche mode'
		for index in range(1,30):
			stories = self.aylienSdkLib.getStories(term, 'NOW-' + str(index) + 'DAYS', 'NOW').stories
			self.assertGreaterEqual(len(stories), 10);
	
	def test3_news_content(self):
		term = 'Aerosmith' 
		news = self.aylienSdkLib.getStories(term, 'NOW-30DAYS', 'NOW')
		for index in range(len(news.stories)):
			body = str(news.stories[index].body.encode('utf-8'),'utf-8')
			print('Testing with: ' + body)
			self.assertIn(term, body)

	def test4_exception_raised_60_one_min_calls(self):
		time.sleep(60)
		lastIndex = 0;
		with self.assertRaises(ApiException) as e:
			term = 'Any Term'
			for index in range(0 , 90):
				print('Call# :' + str(index))
				news = self.aylienSdkLib.getStories(term, 'NOW-30DAYS', 'NOW')
	
if __name__=='__main__':
	unittest.main()