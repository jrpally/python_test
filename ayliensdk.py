import aylien_news_api
import aylienconfiguration
from aylien_news_api.rest import ApiException


class AylienSdk:
	def __init__(self, configuration):
		self.client = aylien_news_api.ApiClient(configuration)
		self.api_instance = aylien_news_api.DefaultApi(self.client)

	def getStories(self, title, published_at_start, published_at_end):
		try:
			api_response = self.api_instance.list_stories(
				title=title, # 'startup',
				published_at_start=published_at_start, #'NOW-7DAYS',
				published_at_end=published_at_end #'NOW'
			)
			return api_response;
		except ApiException as e:
			raise e